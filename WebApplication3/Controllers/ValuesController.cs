﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
	public class ValuesController : ApiController
	{
		private PersonsContext db = new PersonsContext();
		// GET api/values
		[HttpGet]
		public IEnumerable<Person> Get()
		{
			return db.Persons;
		}

		// GET api/values/5
		[HttpGet]
		public Person Get(int id)
		{
			return db.Persons.Find(id);
		}

		// POST api/values
		[HttpPost]
		public void CreatePerson([FromBody]Person person)
		{
			person.Id = db.Persons.Count();
			db.Persons.Add(person);
			db.SaveChanges();
		}

		// PUT api/values/5
		[HttpPut]
		public void EditPerson(int id, [FromBody]Person person)
		{
			if (id==person.Id)
			{
				db.Entry(person).State = EntityState.Modified;
				db.SaveChanges();
			}
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
			Person person = db.Persons.Find(id);
			if (person!=null)
			{
				db.Persons.Remove(person);
				db.SaveChanges();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}

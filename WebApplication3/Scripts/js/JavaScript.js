﻿var listPerson;
var isSortedId = false;
var isSortedName = false;
var isSortedEmail = false;
var isSortedNumber = false;
window.onload = function () {
	getAllPersons();
	document.getElementById('addBtn').onclick = displayBlock;
	document.getElementById("sendAdd").onclick = addPerson;
	document.getElementById("sendEdit").onclick = editPerson;
	document.getElementById("searchBtn").onclick = searchPersons;
};
function getAllPersons() {
	document.getElementById('edit').style.display = "none";
	document.getElementById('add').style.display = "none";
	var xhr = new XMLHttpRequest();
	xhr.open("Get", "api/values/");
	xhr.onreadystatechange = updatePage;
	xhr.send(null);

	function updatePage() {
		if (xhr.status == 200) {
			listPerson = JSON.parse(xhr.responseText);
			WriteResponse(listPerson);
		}
	}
}
function getPerson(id) {
	var xhr = new XMLHttpRequest();
	xhr.open("Get", "api/values/" + id);
	xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
	xhr.onreadystatechange = function () {
		if (xhr.status == 200) {
			showPerson(JSON.parse(xhr.responseText));
		}
	}
	xhr.send();
}
function addPerson() {
	var temp = {
		Name: document.getElementById("addName").value,
		Email: document.getElementById("addEmail").value,
		Number: document.getElementById("addNumber").value
	};
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/api/values/", true);
	xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
	xhr.send(JSON.stringify(temp));
	xhr.onreadystatechange = function () {
		if (this.readyState == 4) {
			getAllPersons();
		};
	};
	//$.ajax({
	//	url: "/api/values/",
	//	type: "POST",
	//	data: JSON.stringify(temp),
	//	contentType: "application/json;charset=utf-8",
	//	success: function (data) {
	//		getAllPersons();
	//	}
	//});

}
function editPerson() {
	var id = document.getElementById("editId").value;
	var person = {
		Id: id,
		Name: document.getElementById("editName").value,
		Email: document.getElementById("editEmail").value,
		Number: document.getElementById("editNumber").value
	};
	var xhr = new XMLHttpRequest();
	xhr.open("Put", "/api/values/" + id, true);
	xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
	xhr.send(JSON.stringify(person));
	xhr.onreadystatechange = function () {
		if (this.readyState == 4) {
			getAllPersons();
		};
	};
}

function removePerson(temp) {
	var id = temp.getAttribute("data-item");
	var xhr = new XMLHttpRequest();
	xhr.open("Delete", "api/values/" + id);
	xhr.setRequestHeader("Content-Type", "application/json,charset=utf-8");
	xhr.onreadystatechange = function () {
		if (this.readyState == 4) {
			getAllPersons();
		};
	};
	xhr.send(null);
}
function sortList(el) {
	event.preventDefault();
	switch (el.text) {
		case "Id":
			{
				if (isSortedId) {
					listPerson.reverse();
					isSortedId = false;
				}
				else {
					listPerson.sort(sortId);
					isSortedId = true;
				}
				break;
			}
		case "Имя":
			{
				if (isSortedName) {
					listPerson.reverse();
					isSortedName = false;
				}
				else {
					listPerson.sort(sortName);
					isSortedName = true;
				}
				break;
			}
		case "Email":
			{
				if (isSortedEmail) {
					listPerson.reverse();
					isSortedEmail = false;
				}
				else {
					listPerson.sort(sortEmail);
					isSortedEmail = true;
				}
				break;
			}
		case "Телефон":
			{
				if (isSortedNumber) {
					listPerson.reverse();
					isSortedNumber = false;
				}
				else {
					listPerson.sort(sortNumber);
					isSortedNumber = true;
				}
				break;
			}
	};
	WriteResponse(listPerson);
};
function sortId(p1, p2) {
	return p1.Id - p2.Id;
}
function sortName(p1,p2) {
	if (p1.Name>p2.Name) {
		return 1;
	}
	if (p1.Name < p2.Name) {
		return -1;
	}
	return 0;
}
function sortEmail(p1, p2) {
	if (p1.Email > p2.Email) {
		return 1;
	}
	if (p1.Email < p2.Email) {
		return -1;
	}
	return 0;
}
function sortNumber(p1, p2) {
	if (p1.Number > p2.Number) {
		return 1;
	}
	if (p1.Number < p2.Number) {
		return -1;
	}
	return 0;
}


function WriteResponse(persons) {
	var str = "<table><th><a href='/' onclick=sortList(this)>Id</a></th>" +
		"<th><a href='/' onclick=sortList(this)>Имя</a></th>" +
		"<th><a href='/' onclick=sortList(this)>Email</a></th>" +
		"<th><a href='/' onclick=sortList(this)>Телефон</a></th>";
	persons.forEach(function (person) {
		str += "<tr><td>" + person.Id + "</td><td>" + person.Name + "</td><td>" + person.Email + "</td><td>" + person.Number +
			"</td><td> <input type='submit' data-item= '" + person.Id + "' id='updateBtn' value = 'Изменить' onclick=displayBlock(this)> </td>" +
			"<td> <input type='submit' data-item='" + person.Id + "' id='removeBtn' value = 'Удалить' onclick=removePerson(this)> </td></tr>"
		;
	});
	str += "</table>";
	document.getElementById("table").innerHTML = str;
}

function displayBlock(temp) {
	var edit = document.getElementById("edit");
	var add = document.getElementById("add");
	var idInput = temp.id;
	switch (idInput) {
		case "updateBtn":
			{
				var id = temp.getAttribute("data-item");
				edit.style.display = "inline-block";
				add.style.display = "none";
				getPerson(id);

				break;
			}
		default:
			{
				add.style.display = "inline-block";
				edit.style.display = "none";
				break;
			}
	}
}
function showPerson(person) {
	document.getElementById("editId").value = person.Id;
	document.getElementById("editName").value = person.Name;
	document.getElementById("editEmail").value = person.Email;
	document.getElementById("editNumber").value = person.Number;
}

function searchPersons() {
	var text = document.getElementById("searchText").value;
	var tempList = [];
	listPerson.forEach(function(el,i) {
		for (var elem in el) {
			var temp = el[elem].toString().indexOf(text);
			if (temp!=-1) {
				tempList.push(el);
				break;
			}
		}
	});
	if (!tempList.length==0) {
		document.getElementById("searchRezult").innerHTML = "Готово";
	} else {
		document.getElementById("searchRezult").innerHTML = "По вашему запросу ничего не найдено";
	}
	
	WriteResponse(tempList);
	
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
	public class PersonInizializator:DropCreateDatabaseAlways<PersonsContext>
	{
		protected override void Seed(PersonsContext context)
		{
			context.Persons.Add(new Person() {Id = 1, Name = "Valera", Email = "1@tut.by", Number = "+375296046450"});
			context.Persons.Add(new Person() { Id = 2, Name = "Mihail", Email = "2@tut.by", Number = "+375292135250" });
			context.Persons.Add(new Person() { Id = 3, Name = "Vadik", Email = "3@tut.by", Number = "+375295645450" });
			context.Persons.Add(new Person() { Id = 4, Name = "Ignat", Email = "4@tut.by", Number = "+375293254450" });
			context.Persons.Add(new Person() { Id = 5, Name = "Vika", Email = "5@tut.by", Number = "+375296542158" });
			base.Seed(context);
		}
	}
}
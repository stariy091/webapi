﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
	public class Person
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Number { get; set; }
	}
}